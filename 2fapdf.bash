#!/usr/bin/env bash

if [[ ! $# -eq 1 ]] ; then
    echo "$0 image_path"
    exit 0
fi

if [ ! -f "$1" ]; then
    echo "No such file."
    exit 0
fi

